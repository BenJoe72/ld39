﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BackgroundNoiseController : MonoBehaviour
{
    public AudioSource RandomNoisePlayer;
    public AudioClip[] RandomClips;
    public float MinClipDelay;
    public float MaxClipDelay;

    private void Start()
    {
        Invoke("PlayRandomNoise", Random.Range(MinClipDelay, MaxClipDelay));
    }

    private void PlayRandomNoise()
    {
        RandomNoisePlayer.Stop();
        RandomNoisePlayer.clip = RandomClips[Random.Range(0, RandomClips.Length)];
        RandomNoisePlayer.Play();
        Invoke("PlayRandomNoise", Random.Range(MinClipDelay, MaxClipDelay));
    }
}