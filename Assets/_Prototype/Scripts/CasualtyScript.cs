﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CasualtyScript : MonoBehaviour
{
    public int maxHealth = 100;

    private Rigidbody _rigidbody;

    public int currentHealth;
    public bool checkEntry;

    private int minImpulseRisk = 1;
    private int maxImpulseRisk = 100;

    void Start ()
    {
        currentHealth = maxHealth;
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Ground" && checkEntry)
        {
            int hurt = (int)(maxHealth * (Mathf.InverseLerp(50, 200, collision.impulse.magnitude)));
            currentHealth = currentHealth - hurt;

            currentHealth = Mathf.Max(currentHealth, 0);

            checkEntry = false;

            if (hurt > 0)
            {
                PeopleAudioController.Instance.PlayHit(this);
            }
        }
    }
}