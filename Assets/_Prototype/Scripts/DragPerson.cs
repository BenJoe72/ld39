﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragPerson : MonoBehaviour
{
    public LayerMask CitizenLayer;
    public TimeUpdater timeUpdater;

    public static DragPerson Instance { get; set; }

    private Transform currentCitizen;
    private float distanceToMouse;
    private SpringJoint springJoint;

    public bool _enabled;

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        springJoint = GetComponent<SpringJoint>();
        distanceToMouse = 100f;
        timeUpdater.timeOverCallback = TimeOut;
    }

    void Update ()
    {
        DragPpl();
    }

    void DragPpl()
    {
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0) && _enabled)
        {
            RaycastHit hit;

            if (Physics.Raycast(mouseRay, out hit, float.PositiveInfinity, CitizenLayer))
            {
                if (hit.transform != null && hit.transform.tag == "Citizen")
                {
                    currentCitizen = hit.transform;
                    Rigidbody citizenBody = currentCitizen.GetComponent<Rigidbody>();
                    springJoint.connectedBody = citizenBody;
                    distanceToMouse = hit.distance;
                    PeopleAudioController.Instance.PlayPickup(currentCitizen.GetComponent<CasualtyScript>());
                }
            }
        }
        else if (currentCitizen != null)
        {
            if (Input.GetMouseButtonUp(0))
            {
                currentCitizen.GetComponent<CasualtyScript>().checkEntry = true;
                springJoint.connectedBody = null;
                currentCitizen = null;
            }
        }

        transform.position = mouseRay.GetPoint(distanceToMouse);
    }

    public void TimeOut()
    {
        _enabled = false;
    }

    public void WinGame()
    {
        _enabled = false;

        if (timeUpdater != null)
        {
            timeUpdater.StopTimer(); 
        }
    }

    public void Killer()
    {
        _enabled = false;

        if (timeUpdater != null)
        {
            timeUpdater.StopTimer();
        }
    }
}