﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; set; }

    public Image PauseMenu;

    public bool didGameEnd;
    public bool didWin;
    public bool started = false;
    public string NextSceneName;
    public int gameTimeInSeconds;
    public float readyTime;
    public float startTime;

    public string continueMessage;
    public string retryMessage;
    public string readyMessage;
    public string startMessage;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        TimeUpdater.Instance.continueText.text = readyMessage;
        TimeUpdater.Instance.continueText.gameObject.SetActive(true);
        Invoke("ReadyText", readyTime);
        DragPerson.Instance._enabled = false;
    }

    void ReadyText()
    {
        TimeUpdater.Instance.continueText.text = startMessage;
        Invoke("StartText", startTime);
    }

    void StartText()
    {
        TimeUpdater.Instance.continueText.gameObject.SetActive(false);
        started = true;
        DragPerson.Instance._enabled = true;
        TimeUpdater.Instance.ResetTimer(gameTimeInSeconds);
    }
    // Update is called once per frame
    void Update()
    {
        if (didGameEnd)
        {
            TimeUpdater.Instance.continueText.text = didWin ? continueMessage : retryMessage;
            TimeUpdater.Instance.continueText.gameObject.SetActive(true);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (didWin)
                {
                    SceneManager.LoadScene(NextSceneName);
                }
                else
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Time.timeScale = !PauseMenu.gameObject.activeSelf ? 0 : 1;
                PauseMenu.gameObject.SetActive(!PauseMenu.gameObject.activeSelf);
            }
        }
    }
}