﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroScript : MonoBehaviour
{
    public float bobSpeed;
    public float bobRange;
    public AnimationCurve bobCurve;
    public bool fallDown;
    public float fallTime;

    private float fallingTime = 0;
    private AudioSource audioSource;
    private bool _enabled;

    public static HeroScript Instance { get; set; }

    private Vector3 originalPosition;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        originalPosition = transform.position;
        audioSource = GetComponent<AudioSource>();
        _enabled = true;
    }

    void Update ()
    {
        if (_enabled)
        {
            if (!fallDown)
            {
                transform.position = new Vector3(originalPosition.x, Mathf.Lerp(originalPosition.y - bobRange / 2, originalPosition.y + bobRange / 2, bobCurve.Evaluate(Time.time / (1 / bobSpeed))), originalPosition.z);
            }
            else
            {
                fallingTime += Time.deltaTime;
                transform.position = new Vector3(originalPosition.x, Mathf.Lerp(originalPosition.y, -2, fallingTime / fallTime), originalPosition.z); 
            }

            if (fallingTime >= fallTime)
            {
                transform.position = new Vector3(originalPosition.x, -2, originalPosition.z);
                audioSource.Play();
                _enabled = false;
            }
        }
    }
}