﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MusicController : MonoBehaviour
{
    public AudioClip menuMusic;
    public AudioClip gameMusic;

    private AudioSource source;

    void Start ()
    {
        DontDestroyOnLoad(this);
        source = GetComponent<AudioSource>();
    }

    void OnLevelWasLoaded(int level)
    {
        if (GameController.Instance != null)
        {
            if (source.clip != gameMusic)
            {
                source.Stop();
                source.clip = gameMusic;
                source.Play(); 
            }
        }
        else
        {
            if (source.clip != menuMusic)
            {
                source.Stop();
                source.clip = menuMusic;
                source.Play();
            }
        }
    }
}