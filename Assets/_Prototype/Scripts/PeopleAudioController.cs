﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PeopleAudioController : MonoBehaviour
{
    public static PeopleAudioController Instance { get; set; }

    public AudioClip[] PickupAudios;
    public AudioClip[] HitAudios;

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void PlayPickup(CasualtyScript person)
    {
        AudioSource audioSource = person.GetComponent<AudioSource>();
        audioSource.Stop();
        audioSource.clip = PickupAudios[UnityEngine.Random.Range(0, PickupAudios.Length)];
        audioSource.Play();
    }

    public void PlayHit(CasualtyScript person)
    {
        AudioSource audioSource = person.GetComponent<AudioSource>();
        audioSource.Stop();
        audioSource.clip = HitAudios[UnityEngine.Random.Range(0, HitAudios.Length)];
        audioSource.Play();
    }
}