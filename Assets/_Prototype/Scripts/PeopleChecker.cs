﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PeopleChecker : MonoBehaviour
{
    public PeopleUpdater pplUpdater;
    public DragPerson dragPerson;
    public CapsuleCollider[] ppl;

    private BoxCollider _collider;
    private bool _enabled;

    void Start ()
    {
        pplUpdater.ResetPplCount(ppl.Length);
        _collider = GetComponent<BoxCollider>();
        _enabled = true;
    }
    
    void Update ()
    {
        if (_enabled && !GameController.Instance.didGameEnd)
        {
            int savedPpl = ppl.Length - CheckforPpl();
            pplUpdater.UpdatePplText(savedPpl);

            if (savedPpl == ppl.Length)
            {
                _enabled = false;
                dragPerson.WinGame();
                GameController.Instance.didGameEnd = true;
                GameController.Instance.didWin = true;
            }
        }
    }

    private int CheckforPpl()
    {
        int result = 0;

        foreach (var person in ppl)
        {
            if (_collider.bounds.Intersects(person.bounds))
            {
                result++;
            }
        }

        return result;
    }
}