﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    [Header("References")]
    public SpriteRenderer Background;
    public SpriteRenderer StrongGuy;
    public SpriteRenderer BottomBar;
    public SpriteRenderer Title;
    public Text continueText;
    public Image blackPanel;
    
    private Vector3 StrongGuyEndPlace;
    private Vector3 BottomBarEndPlace;
    private Vector3 TitleEndPlace;
    
    private Vector3 StrongGuyStartPlace;
    private Vector3 BottomBarStartPlace;
    private Vector3 TitleStartPlace;

    [Header("Values")]
    public float BarFloatTime;
    public float BackgroundTime;
    public float fadeOutTime;

    [Header("Data")]
    public string NextSceneName;

    void Start()
    {
        StrongGuyEndPlace = new Vector3(4, -2.5f, StrongGuy.transform.position.z);
        BottomBarEndPlace = new Vector3(0, -2, BottomBar.transform.position.z);
        TitleEndPlace = new Vector3(0, 0, Title.transform.position.z);
        
        StrongGuyStartPlace = StrongGuy.transform.position;
        TitleStartPlace = Title.transform.position;
        BottomBarStartPlace = BottomBar.transform.position;

        blackPanel.color = new Color(blackPanel.color.r, blackPanel.color.g, blackPanel.color.b, 0);

        StartCoroutine(SlideInItems());

        continueText.gameObject.SetActive(false);
    }

    private IEnumerator SlideInItems()
    {
        float loadTime = 0f;
        float allLoadTime = BarFloatTime + BackgroundTime;
        while (loadTime <= allLoadTime)
        {
            if (loadTime < BarFloatTime)
            {
                // float bars from side
                BottomBar.transform.position = Vector3.Lerp(BottomBarStartPlace, BottomBarEndPlace, loadTime / BarFloatTime);
            }
            else
            {
                // float guy from bottom, fade background
                BottomBar.transform.position = BottomBarEndPlace;
                StrongGuy.transform.position = Vector3.Lerp(StrongGuyStartPlace, StrongGuyEndPlace, (loadTime - BarFloatTime) / BackgroundTime);
                Title.transform.position = Vector3.Lerp(TitleStartPlace, TitleEndPlace, (loadTime - BarFloatTime) / BackgroundTime);
            }

            loadTime += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        StrongGuy.transform.position = StrongGuyEndPlace;

        Invoke("ShowContinueText", 3f);
    }

    private void ShowContinueText()
    {
        continueText.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(LoadNextScene());
        }
    }

    private IEnumerator LoadNextScene()
    {
        float colorTimer = 0;
        while (colorTimer < fadeOutTime)
        {
            blackPanel.color = new Color(blackPanel.color.r, blackPanel.color.g, blackPanel.color.b, Mathf.Lerp(0, 1, colorTimer / fadeOutTime));

            colorTimer += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        SceneManager.LoadScene(NextSceneName);
    }
}