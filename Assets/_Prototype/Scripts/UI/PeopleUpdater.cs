﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PeopleUpdater : MonoBehaviour
{
    public Text peopleText;
    public Text winText;
    public Text killText;
    public Slider casualtySlider;
    public int allowedCasualtyNumber;
    public int allCasualty;

    public CasualtyScript[] casualtyScripts;

    private string baseString = "{0}/{1} saved";
    private int maxPpl;

    public void ResetPplCount(int maxpplcount)
    {
        maxPpl = maxpplcount;
        UpdatePplText(0);
        winText.gameObject.SetActive(false);
        killText.gameObject.SetActive(false);
    }

    public void UpdatePplText(int currentSavedPpl)
    {
        peopleText.text = string.Format(baseString, currentSavedPpl, maxPpl);

        if (currentSavedPpl == maxPpl)
        {
            winText.gameObject.SetActive(true);
        }
    }

    private void Update()
    {
        allCasualty = casualtyScripts.Sum(x => x.maxHealth - x.currentHealth);
        float casualtyRate = allCasualty / (float)allowedCasualtyNumber;
        casualtySlider.value = casualtyRate;

        if (casualtyRate >= 1 && !GameController.Instance.didGameEnd)
        {
            killText.gameObject.SetActive(true);
            DragPerson.Instance.Killer();
            GameController.Instance.didGameEnd = true;
            GameController.Instance.didWin = false;
        }
    }
}