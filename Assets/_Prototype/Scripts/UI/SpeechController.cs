﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpeechController : MonoBehaviour
{
    [Header("References")]
    public SpriteRenderer Background;
    public SpriteRenderer TopBar;
    public SpriteRenderer StrongGuy;
    public SpriteRenderer BottomBar;
    public SpriteRenderer SpeechBubble;
    public SpriteRenderer SkipButton;
    public Text SpeechBubbleText;

    private Vector3 TopBarEndPlace;
    private Vector3 StrongGuyEndPlace;
    private Vector3 BottomBarEndPlace;
    private Vector3 SpeechBubbleEndPlace;

    private Vector3 TopBarStartPlace;
    private Vector3 StrongGuyStartPlace;
    private Vector3 BottomBarStartPlace;
    private Vector3 SpeechBubbleStartPlace;

    [Header("Values")]
    public float BarFloatTime;
    public float BackgroundTime;
    public float SpeechBubbleTime;

    [Header("Data")]
    [Multiline]
    public string[] SpeechTexts;
    public float TextSpeed;
    public string NextSceneName;

    private Queue<string> _speechQueue;
    //private Color startColor;
    //private Color endColor;
    private bool skipEnabled;

    void Start ()
    {
        //startColor = new Color(255, 255, 255, 0);
        //endColor = new Color(255, 255, 255, 255);
        SpeechBubbleText.gameObject.SetActive(false);

        TopBarEndPlace = new Vector3(0, 0, TopBar.transform.position.z);
        StrongGuyEndPlace = new Vector3(0, 0, StrongGuy.transform.position.z);
        BottomBarEndPlace = new Vector3(0, 0, BottomBar.transform.position.z);
        SpeechBubbleEndPlace = new Vector3(0, 0, SpeechBubble.transform.position.z);

        TopBarStartPlace = TopBar.transform.position;
        StrongGuyStartPlace = StrongGuy.transform.position;
        BottomBarStartPlace = BottomBar.transform.position;
        SpeechBubbleStartPlace = SpeechBubble.transform.position;

        _speechQueue = new Queue<string>(SpeechTexts);

        StartCoroutine(SlideInItems());
    }

    private IEnumerator SlideInItems()
    {
        float loadTime = 0f;
        float allLoadTime = BarFloatTime + BackgroundTime + SpeechBubbleTime;
        while (loadTime <= allLoadTime)
        {
            if (loadTime < BarFloatTime)
            {
                // float bars from side
                TopBar.transform.position = Vector3.Lerp(TopBarStartPlace, TopBarEndPlace, loadTime / BarFloatTime);
                BottomBar.transform.position = Vector3.Lerp(BottomBarStartPlace, BottomBarEndPlace, loadTime / BarFloatTime);
            }
            else if (loadTime < BarFloatTime + BackgroundTime)
            {
                // float guy from bottom, fade background
                TopBar.transform.position = TopBarEndPlace;
                BottomBar.transform.position = BottomBarEndPlace;
                StrongGuy.transform.position = Vector3.Lerp(StrongGuyStartPlace, StrongGuyEndPlace, (loadTime - BarFloatTime) / BackgroundTime);
                //Background.color = Color.Lerp(startColor, endColor, loadTime - BarFloatTime / BackgroundTime);
            }
            else
            {
                // float speech bubble from bottom
                StrongGuy.transform.position = StrongGuyEndPlace;
                SpeechBubble.transform.position = Vector3.Lerp(SpeechBubbleStartPlace, SpeechBubbleEndPlace, (loadTime - BarFloatTime - BackgroundTime) / SpeechBubbleTime);
            }


            loadTime += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        SpeechBubble.transform.position = SpeechBubbleEndPlace;
        SpeechBubbleText.text = string.Empty;
        SpeechBubbleText.gameObject.SetActive(true);
        
        if (_speechQueue.Count > 0)
        {
            StartCoroutine(StartTexts(_speechQueue.Dequeue()));
        }
    }

    private IEnumerator StartTexts(string text)
    {
        SpeechBubbleText.text = string.Empty;
        foreach (var character in text)
        {
            SpeechBubbleText.text += character;
            yield return new WaitForSeconds(1 / TextSpeed);
        }

        while (!Input.GetKey(KeyCode.Space))
        {
            yield return new WaitForEndOfFrame();
        }
        
        if (_speechQueue.Count > 0)
        {
            StartCoroutine(StartTexts(_speechQueue.Dequeue()));
        }
        else
        {
            LoadNextScene();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (skipEnabled)
            {
                LoadNextScene();
            }
            else
            {
                skipEnabled = true;
                SkipButton.gameObject.SetActive(true);
            }
        }
    }

    private void LoadNextScene()
    {
        SceneManager.LoadScene(NextSceneName);
    }
}