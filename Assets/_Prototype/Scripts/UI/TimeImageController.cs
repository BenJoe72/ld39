﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeImageController : MonoBehaviour
{
    public Image Min1;
    public Image Min2;
    public Image Sec1;
    public Image Sec2;

    public Sprite[] numberSprites;

    public static TimeImageController Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void SetTime(int sec)
    {
        int min = sec / 60;
        int realSec = sec % 60;

        int min1 = min / 10;
        int min2 = min % 10;
        int sec1 = realSec / 10;
        int sec2 = realSec % 10;

        Min1.sprite = numberSprites[min1];
        Min2.sprite = numberSprites[min2];
        Sec1.sprite = numberSprites[sec1];
        Sec2.sprite = numberSprites[sec2];
    }
}