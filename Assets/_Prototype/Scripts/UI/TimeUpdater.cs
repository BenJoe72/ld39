﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeUpdater : MonoBehaviour
{
    public Text timeUpText;
    public Slider timerSlider;
    public Action timeOverCallback;
    public Text continueText;

    private int maxSeconds;
    private int currentSeconds;

    public static TimeUpdater Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        timeUpText.gameObject.SetActive(false);
        UpdateTime();
    }

    public void ResetTimer(int seconds)
    {
        maxSeconds = seconds;
        currentSeconds = seconds;
        StartCoroutine(SecondCountDown());
        UpdateTime();
    }

    private void UpdateTime()
    {
        TimeImageController.Instance.SetTime(currentSeconds);
        timerSlider.value = currentSeconds / (float)maxSeconds;
    }

    public void StopTimer()
    {
        StopAllCoroutines();
    }

    private IEnumerator SecondCountDown()
    {
        while(currentSeconds > 0)
        {
            yield return new WaitForSeconds(1);
            currentSeconds -= 1;
            UpdateTime();
        }

        if (timeOverCallback != null && !GameController.Instance.didGameEnd)
        {
            timeOverCallback();
            GameController.Instance.didGameEnd = true;
            GameController.Instance.didWin = false;
            HeroScript.Instance.fallDown = true;
        }

        timeUpText.gameObject.SetActive(true);
    }
}